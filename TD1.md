# TD1 - D'un code simple à un code versionné et documenté 
<div style="text-align:center"><img src="figs/intersection_crible.png" width="40%"></div>

---

Le but de ce TD est de partir de l'exemple simple du cours "criblage" et de réaliser ensemble les différentes étapes de développement. L'implémentation du code n'est pas importante ici, les codes présentés lors du CI2 sur la compilation seront utilisés. Pour rappel, ces codes servent à déterminer l'aire de l'intersection entre deux cercles, via la méthode du criblage. 

## 1 - Début du versionnement 

- Créer un répertoire vide, lié à un dépôt GitLab (`git init, ou git clone`). Pour plus de simplicité:  

```
    git clone https://gitlab.com/qgoestch/td1-criblage.git
```
- Déposer les fichiers du code de criblage correspondant à la version 1 (brutale) du CI2 : **fichier_unique**.
- Compiler le code avec gcc. Tester la différence entre `gcc -o main main.c` et `gcc -o main main.c -lm`. 
- Faites un premier commit.

## 2 - Créer un readme 
- Créer un fichier .md et y renseigner les informations relatives au projet.
- Faites un deuxième commit.

## 3 - Améliorer les codes de criblage avec des fichiers séparés 
- Utiliser les ressources du CI2 **fichier_separes** pour voir les différences entre les deux versions.
- Utiliser les commandes `git diff` ou `git status`.
- Commit.

## 4 - Utiliser un makefile
- Utiliser un makefile simple de la version **fichiers_separes_meilleurs** et les codes correspondants.
- Regarder ce que donne l'execution du makefile lorsque l'un des fichiers sources  __*.c__ est modifié. 


## 5 - Utiliser des répertoires séparés 
- Remplacer les code existants par les code de la version **repertoires_separes**. 
- Analyser le makefile.

## 6 - Créer une documentation doxygen 
- Executer la commande `doxygen -g`. Installer doxygen si ce n'est pas fait. 
- Modifier le fichier Doxyfile généré pour qu'il aille chercher recursivement dans les sous-dossiers.
- Utiliser la documentation [Doxygen](https://www.doxygen.nl/manual/index.html) pour arriver au résultat voulu.
- Commit.


## 7 - Mettez en place l'intégration continue pour la documentation 
- Utilisation de [Gitlab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).
- Suivre ce [tutoriel](https://gitlab.com/pages/doxygen).


