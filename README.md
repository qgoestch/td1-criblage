# Criblage
---
Auteur : groupe 1 TechDev

La [documentation](https://qgoestch.gitlab.io/td1-criblage/index.html) de ce projet est générée via CI (_continious integration_).

## Utilité: 

Determiner l'aire d'intersection entre deux cercles 

<div style="text-align:center"><img src="figs/intersection_crible.png" width="30%"></div>

## Fichiers principaux:

- main.c
- rectangle.c
- cercle.c 
- point.c 

## Dépendances: 

- librairie <math.h>
- librairie <stiod.h>


