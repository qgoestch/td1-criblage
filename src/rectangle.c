/** 
 * \file rectangle.c 
 * code pour les rectangles
 * \author Basile Sauvage
 */

#include "../include/rectangle.h"

/**
 * \relatesalso str_rectangle
 * \param c1 cercle
 * \param c2 cercle
 * \return le plus petit rectangle (aligné sur les axes) encadrant \a c1 et \a c2
 */
rectangle rectangle_encadrant(cercle c1, cercle c2){
	rectangle r = {{xmin(c1,c2), ymin(c1,c2)}, {xmax(c1,c2), ymax(c1,c2)}};
	return r;
}

/**
 * \relatesalso str_rectangle
 * \param rect rectangle
 * \return l'aire de \a rect (\c float)
 */
float surface_rectangle(rectangle rect){
	return (rect.xmax_ymax.x  - rect.xmin_ymin.x) * (rect.xmax_ymax.y  - rect.xmin_ymin.y);
}

/**
 * \param min (borne inférieure)
 * \param max (borne supérieure)
 * \return une valeur aléatoire entre \a min et \a max (\c float)
 * \pre \a min < \a max
 */
float aleatoire(float min, float max){
	return (float) rand() * (max-min) / (float) RAND_MAX + min;
}

/**
 * \relatesalso str_rectangle
 * \param rect rectangle
 * \return un point aléatoire dans \a rect
 */
point point_aleatoire(rectangle rect){
	point p = {aleatoire(rect.xmin_ymin.x,rect.xmax_ymax.x) , aleatoire (rect.xmin_ymin.y,rect.xmax_ymax.y)};
	return p;
}
