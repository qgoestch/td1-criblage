/** 
 * \file point.c 
 * code pour les points
 * \author Basile Sauvage
 */

#include "../include/point.h"

/**
 * calcule la distance entre deux points
 * \relatesalso str_point
 * \param pt1 un point
 * \param pt2 un point
 * \returns la distance euclidienne entre \a pt1 et \a pt2
 */
float distance(point pt1, point pt2){
	float dx = pt1.x - pt2.x;
	float dy = pt1.y - pt2.y;
	return sqrt(dx*dx+dy*dy);
}

/**
 * affiche les coordonnées d'un point dans la console.
 * \relatesalso str_point
 * \param p un point
 * \returns \c void
 */
void print_point (point p){
	printf("{\t%.2f\t%.2f\t}\n",p.x,p.y);
}

