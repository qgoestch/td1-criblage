/** 
 * \file cercle.c 
 * code pour les cercles
 * \author Basile Sauvage
 */

#include "../include/cercle.h"

/** 
 * \fn float xmin(cercle c1, cercle c2);
 * \relatesalso str_cercle
 * \param c1 premier cercle
 * \param c2 deuxième cercle
 * \return l'abscisse minimale de tous les points de deux cercles
 */
float xmin(cercle c1, cercle c2){
	return min(c1.centre.x - c1.rayon, c2.centre.x - c2.rayon);
}


/** 
 * \fn  float ymin(cercle c1, cercle c2);
 * \relatesalso str_cercle
 * \param c1 premier cercle
 * \param c2 deuxième cercle
 * \return l'ordonnée minimale de tous les points de deux cercles
 */
float ymin(cercle c1, cercle c2){
	return min(c1.centre.y - c1.rayon, c2.centre.y - c2.rayon);
}

/** 
 * \fn float xmax(cercle c1, cercle c2);
 * \relatesalso str_cercle
 * \param c1 premier cercle
 * \param c2 deuxième cercle
 * \return l'abscisse maximale de tous les points de deux cercles
 */
float xmax(cercle c1, cercle c2){
	return min(c1.centre.x + c1.rayon, c2.centre.x + c2.rayon);
}

// modif mineur
/** 
 * \relatesalso str_cercle
 * \param c1 premier cercle
 * \param c2 deuxième cercle
 * \return l'ordonnée maximale de tous les points de deux cercles
 */
float ymax(cercle c1, cercle c2){
	return min(c1.centre.y + c1.rayon, c2.centre.y + c2.rayon);
}

/** 
 * \relatesalso str_cercle
 * \param pt un joli point
 * \param cerc un cercle
 * \return booléen = VRAI ssi \a pt appartient à \a cerc
 */
int appartient(point pt, cercle cerc){
	return (distance(pt,cerc.centre) <= cerc.rayon);
}
