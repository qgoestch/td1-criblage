/** 
 * \file main.c 
 * code principal
 * \author Groupe 1 TechDev
 */

#include "../include/point.h" // techniquement facultatif, mais obligatoire par clarté
#include "../include/cercle.h" // techniquement facultatif, mais obligatoire par clarté
#include "../include/rectangle.h"
#include <stdio.h>

int main () {
	/** 
	 * Routine principale de test.
 	*/
	point p1 = {2.,2.};
	point p2 = {2.,3.};
	cercle c1 = {p1, 1.};
	cercle c2 = {p2, 1.};
	
	rectangle r = rectangle_encadrant(c1,c2);
	
	srand(time(NULL));
	int i;
	point p;
	int n = 100000;
	int cpt = 0;
	for (i = 0; i<n; ++i){
		p = point_aleatoire (r);
		if (appartient(p,c1) & appartient (p,c2))
			cpt++;
	}
	printf ("surface d'intersection : %.5f\n",(float) cpt / n * surface_rectangle(r));
	return 0;

}
