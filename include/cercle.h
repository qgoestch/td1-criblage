/** 
 * \file  
 * header pour les cercles
 * \author Basile Sauvage
 */

#ifndef __CERCLE_H // indispensable pour éviter les déclarations multiples
#define __CERCLE_H 



#include "point.h"

/**
 * \struct str_cercle
 * Déclaration de la structure de cercle
 */
struct str_cercle 
{
  point centre; /**< centre du cercle*/
  float rayon; /**< rayon du cercle*/
 };
 
/**
 * \typedef typedef struct str_cercle cercle;
 * type synonyme de \c struct \c str_cercle
 */
typedef struct str_cercle cercle;

float xmin(cercle c1, cercle c2);
float ymin(cercle c1, cercle c2);
float xmax(cercle c1, cercle c2);
float ymax(cercle c1, cercle c2);
int appartient(point pt, cercle cerc);

#endif
