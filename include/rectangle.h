/** 
 * \file rectangle.h 
 * header pour les rectangles
 * \author Basile Sauvage
 */

#ifndef __RECTANGLE_H // indispensable pour éviter les déclarations multiples
#define __RECTANGLE_H 

#include "point.h" // techniquement facultatif, mais obligatoire par clarté
#include "cercle.h"
#include <time.h>

/**
 * \struct str_rectangle
 * Déclaration de la structure de rectangle
 */
struct str_rectangle 
{
  point xmin_ymin; /**< coin inférieur gauche du rectangle */
  point xmax_ymax; /**< coin supérieur droit du rectangle */
};

/**
 * \typedef rectangle
 * type synonyme de \c struct \c str_rectangle
 */
typedef struct str_rectangle rectangle;

rectangle rectangle_encadrant(cercle c1, cercle c2);
float surface_rectangle(rectangle rect);

float aleatoire(float min, float max);
point point_aleatoire(rectangle rect);

#endif
