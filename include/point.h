/** 
 * \file point.h 
 * header pour les points
 * \author BGroupe 1 TechDev
 */

#ifndef __POINT_H // indispensable pour éviter les déclarations multiples
#define __POINT_H
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

float static min (float x, float y) {return (x<y?x:y);} /**< renvoie le minimum de deux flottants */
float static max (float x, float y) {return (x<y?y:x);} /**< renvoie le maximum de deux flottants */

/**
 * \struct str_point
 * déclaration de la structure de point
 */
struct str_point 
{
   float x; /**< abscisse du point */
   float y; /**< ordonnée du point */
 };
 
/**
 * \typedef typedef struct str_point point;
 * type synonyme de \c struct \c str_point
 */
typedef struct str_point point;

float distance(point pt1, point pt2);
void print_point (point p);

#endif
